//Package graphic is using the sdl2 go interface from (c)https://github.com/veandco/go-sdl2/ under the BSD 3 License
package graphic

import (
	"container/list"
	"fmt"
	"log"
	"strconv"

	"github.com/veandco/go-sdl2/sdl"
)

//Window contains the information required to render a window with diffrent Sprites
type Window struct {
	Sprites    []Sprite
	background struct {
		Texture      *sdl.Texture
		SRect, DRect sdl.Rect
		W, H         int32
	}
	DrawColor struct {
		r, b, g, a uint8
	}
	Renderer *sdl.Renderer
	window   *sdl.Window
	fps      uint32
	logger   log.Logger
}

//IsInit returns if window was initialized
func (window *Window) IsInit() bool {
	return !(window.Renderer == nil || window.window == nil || window.Sprites == nil)
}

//Render renders the information from the window object to the screen
func (window *Window) Render() error {

	if !window.IsInit() {
		return fmt.Errorf("Window not initialized")
	}

	err := window.Renderer.SetDrawColor(window.DrawColor.r, window.DrawColor.g, window.DrawColor.b, window.DrawColor.a)
	if err != nil {
		window.logger.Println(err)
		return err
	}
	err = window.Renderer.Clear()
	if err != nil {
		window.logger.Println(err)
		return err
	}

	if window.background.W != 0 {
		err = window.Renderer.Copy(window.background.Texture, &window.background.SRect, &window.background.DRect)
		if err != nil {
			window.logger.Println(err)
			return err
		}
	}

	for _, i := range window.Sprites {
		for j := i.instances.Front(); j != nil; j = j.Next() {
			if instance, ok := j.Value.(*Instance); ok {
				err = window.Renderer.CopyExF(i.texture, &i.srcRect, &instance.DestRect, instance.Angle, &instance.Center, sdl.FLIP_HORIZONTAL)
				if err != nil {
					window.logger.Println(err)
					return err
				}
			} else {
				fmt.Println("list of sprite does not contain Instances")
			}
		}
	}
	window.Renderer.Present()
	return nil
}

//New returns a Window object with initialized renderer and window note that Sprites have to be added manual
func (window *Window) Init(config WindowConfig) error {
	err := InitLogger(&window.logger, config.Title)
	if err != nil {
		window.logger.Println(err)
		fmt.Println("logs are now disabled")
	}

	window.Sprites = make([]Sprite, 0)

	err = sdl.Init(sdl.INIT_VIDEO | sdl.INIT_TIMER)
	if err != nil {
		window.logger.Println(err)
		return err
	}

	window.window, err = sdl.CreateWindow(config.Title, config.X, config.Y, config.Width, config.Height, config.WindowFlags)
	if err != nil {
		sdl.QuitSubSystem(sdl.INIT_VIDEO | sdl.INIT_TIMER)
		window.logger.Println(err)
		return err
	}

	window.Renderer, err = sdl.CreateRenderer(window.window, -1, config.RendererFlags)

	if err != nil {
		sdl.QuitSubSystem(sdl.INIT_VIDEO | sdl.INIT_TIMER)
		window.logger.Println(err)
		return err
	}

	window.fps = config.FPS
	window.logger.Println("Created Window " + config.Title + " succesfully")
	return nil
}

func (window *Window) Quit() {
	if !window.IsInit() {
		return
	}

	for _, i := range window.Sprites {
		i.texture.Destroy()
	}

	sdl.Quit()
}

//LoadSprites from config object returns map with sprite IDs linked to the sprite name
func (window *Window) LoadSprites(config []SpriteBaseConfig) (map[string]int, error) {
	if !window.IsInit() {
		return nil, fmt.Errorf("Window not initialized")
	}
	spriteIDs := make(map[string]int)

	for _, i := range config {
		spriteID, err := window.AddSprite(i.ImgPath, i.Sprites[0].SrcRect)
		spriteIDs[i.Sprites[0].Name] = spriteID
		if err != nil {
			window.logger.Println("called from LoadSprites")
			return spriteIDs, err
		}

		for _, j := range i.Sprites[1:] {
			spriteIDs[j.Name] = window.AddSpriteByID(spriteID, j.SrcRect)
		}
	}

	return spriteIDs, nil
}

//LoadAndDumpSprites load new sprites and dump every old sprite and instance
func (window *Window) LoadAndDumpSprites(config []SpriteBaseConfig) (map[string]int, error) {
	window.Sprites = make([]Sprite, 0)
	return window.LoadSprites(config)
}

//AddSprite adds another sprite which can be used be creating a instance of it see Sprite.NewInstance
func (window *Window) AddSprite(imgPath string, srcRect sdl.Rect) (int, error) {
	var err error
	var sprite Sprite
	if !window.IsInit() {
		return -1, fmt.Errorf("Window not initialized")
	}
	retIndex := len(window.Sprites)

	sprite, err = NewSprite(window.Renderer, imgPath, srcRect)
	if err != nil {
		window.logger.Println(err)
		return -1, err
	}
	window.Sprites = append(window.Sprites, sprite)

	window.logger.Println("added Sprite " + imgPath + " succesfully the ID is " + strconv.Itoa(retIndex))

	return retIndex, nil
}

//AddSpriteByID adds another sprite with the same texture as sprite with id spriteID
func (window *Window) AddSpriteByID(spriteID int, srcRect sdl.Rect) int {
	var sprite Sprite

	if !window.IsInit() {
		return -1
	}

	if spriteID < 0 {
		window.logger.Println("sprite ID below 0")
		return -1
	}

	if len(window.Sprites)-1 < spriteID {
		window.logger.Println("sprite: ", spriteID, " does not exist")
		return -1
	}

	sprite.texture = window.Sprites[spriteID].texture
	sprite.srcRect = srcRect
	sprite.instances = list.New()

	retIndex := len(window.Sprites)
	window.Sprites = append(window.Sprites, sprite)

	window.logger.Println("added Sprite by ID " + strconv.Itoa(int(spriteID)) + " succesfully the ID is " + strconv.Itoa(retIndex))

	return retIndex
}

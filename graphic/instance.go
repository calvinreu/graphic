package graphic

import "github.com/veandco/go-sdl2/sdl"

//Instance position Angle and the Center of an instance of a sprite
type Instance struct {
	DestRect sdl.FRect
	Angle    float64
	Center   sdl.FPoint
}

//NewPosition sets the position of this instance Center is the Center of the instances new position
func (instance *Instance) NewPosition(x, y float32) {
	instance.DestRect.X = x
	instance.DestRect.Y = y
}

//ChangePosition moves the instance by x, y
func (instance *Instance) ChangePosition(x, y float32) {
	instance.DestRect.X += x
	instance.DestRect.Y += y
}
